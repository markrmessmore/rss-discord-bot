var   discordBot        = require("./app.js"),
      RssFeedEmitter    = require('rss-feed-emitter'),
      dota2             = new RssFeedEmitter(),
      wld               = new RssFeedEmitter();
  //    feeder3           = new RssFeedEmitter();

exports.dota2 = function () {
  console.log("Listening for Dota 2 Updates!");

 dota2.add({
   url: 'https://rss.app/feeds/B85PrOfBkNLkIeAc.xml',
 });

 dota2.on('new-item', item => {
   item.img = 'https://i.imgur.com/VMDEMw8.png'
   discordBot.rss(item)
 })
};

exports.wld = () => {
  console.log("Listening for WeLikeDota Updates!");

  wld.add({
    url: 'http://feeds.feedburner.com/welikedota',
  });

  wld.on('new-item', item => {
    item.img = 'https://i.imgur.com/gpOlJtD.png'
    discordBot.rss(item)
  })
};

// exports.rssFeed3 = function (settings, Discord, discordClient) {
//   console.log("Listening for YouTube Updates!");

//   feeder3.add({
//     url: 'https://www.youtube.com/feeds/videos.xml?channel_id=UCqkv51wCAe5KwOWJxKQ6Tww',
//   });

//   feeder3.on('new-item', item => {
//     item.title = `New YouTube Vid: ${item.title}`
//     item.img = 'https://yt3.ggpht.com/a/AGF-l7-M_LnNrLUnmoAT6ugGGydERHu1nwfDS9DaVA=s288-c-k-c0xffffffff-no-rj-mo'
//     discordBot.rss(settings, Discord, discordClient, item)
//   })
// };
