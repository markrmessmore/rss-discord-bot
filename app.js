const Discord       = require("discord.js"),
      discordClient = new Discord.Client(),
      moment        = require('moment'),
      rssBot        = require("./rssBot.js"),
      settings      = require("./settings.json");


// START'ER UP

discordClient.login(settings.discordToken)

discordClient.on('ready', () => {
      console.log("Discord is ready to rock!");
      discordClient.user.setActivity('for updates.', {type: 'WATCHING'})
      rssBot.wld(settings)
      rssBot.dota2(settings)
});

exports.rss = function (msg) {
      let titles      = new Set()
      let limitDate   = moment().subtract(7, 'days')
      let dChannel    = discordClient.channels.cache.get(settings.guildPrimaryChannel)
      //IF THE DATE OF THE POST IS LESS THAN 7 DAYS OLD...REVIEW
      if (limitDate < moment(msg.date)){
            dChannel.messages.fetch()
            .then(res => {
                  for (let item of res.values()){
                  //GATHER THE LAST 100 POSTS FROM THE CHANNEL POSTED BY THE BOT, ADD THEM TO THE SET
                        if (item.author.username == 'Dota & WLD Updates' && item.embeds.length > 0) { 
                              titles.add(item.embeds[0].title)
                        }
                  }
            })
            .then(() => {
                  //NOW THAT WE HAVE ALL THE TITLES IN THE SET, WE CHECK IF THIS TITLE IS PRESENT. IF IT IS NOT, WE SEND IT TO THE CHANNEL
                  if (!titles.has(`***${msg.title}***`)) {
                        let rssEmbed = new Discord.MessageEmbed()
                              .setTitle(`***${msg.title}***`)
                              .setThumbnail(`${msg.img}`)
                              .setColor(3447003)
                              .addField("Update URL:", `${msg.link}`)
                              .addField("Published:", `${msg.pubDate}`);
                        dChannel.send(rssEmbed)
                  }
            })
            .catch(err => console.log(err))
      }
};
